package it.unibg.so.lab2;


public class CircularBuffer implements Buffer<Integer> {
	
	private Integer[] items;
	private int currentPosition;
	private int lastInsertion;
	
	public CircularBuffer(int size){
		items = new Integer[size];
		currentPosition = 0;
		lastInsertion = 0;
		for(int i=0; i<size; i++)
			items[i]=null;
	}

	@Override
	public synchronized void push(Integer item) {
		System.out.println("[Th-" + Thread.currentThread().getId() + "] push: " + item);
		if(lastInsertion == currentPosition && items[currentPosition] != null){
			try {
				System.out.println("[Th-" + Thread.currentThread().getId() + "] wait");
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		items[currentPosition] = item;
		incCurrentPosition();
		notifyAll();
		System.out.println("[Th-" + Thread.currentThread().getId() + "] push finish");
	}

	@Override
	public synchronized Integer pop() {
		System.out.println("[Th-" + Thread.currentThread().getId() + "] pop");
		if(isEmpty()){
			try {
				System.out.println("[Th-" + Thread.currentThread().getId() + "] wait");
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		int ret = items[lastInsertion];
		items[lastInsertion] = null;
		incLastPosition();
		notifyAll();
		System.out.println("[Th-" + Thread.currentThread().getId() + "] pop finish");
		return ret;
	}

	@Override
	public synchronized boolean isEmpty() {
		return lastInsertion == currentPosition && items[currentPosition] == null;
	}
	
	private synchronized void incCurrentPosition(){
		currentPosition = (currentPosition + 1) % items.length;
	}
	
	private synchronized void incLastPosition(){
		lastInsertion = (lastInsertion + 1) % items.length;
	}
	
	@Override
	public String toString(){
		String ret = "";
		for(int i=0; i<items.length; i++)
			ret += items[i] + ", ";
		return "buffer: [" + ret.trim() + "]";
	}

}
