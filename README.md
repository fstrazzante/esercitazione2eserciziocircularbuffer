# CIRCULAR BUFFER EXAMPLE #

Un buffer circolare viene inizializzato dando in input la lunghezza desiderata

operazione di `push` (inserimento di un elemento):

* il primo inserimento parte nella posizione 0, poi 1, 2, e così via
* quando il buffer è pieno, l’inserimento di un elemento va a sostituire l’elemento più vecchio contenuto nel buffer

operazione di `pop` (rimozione di un elemento):

* l’operazione rimuove l’elemento più vecchio contenuto nel buffer
* se il buffer è vuoto viene generata una opportuna eccezione

operazione `isEmpty`:

* ritorna true se il buffer è vuoto, false altrimenti

Usando gli opportuni meccanismi di sincronizzazione (`synchronized`, `wait`, `notify`), le operazioni eseguite sulla struttura dati avvengano in mutua esclusione.